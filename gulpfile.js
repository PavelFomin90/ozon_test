"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const prefix = require("gulp-autoprefixer");
const rename = require("gulp-rename");
const svgo = require("gulp-svgo");
const copy = require("gulp-copy");

gulp.task("style", () => {
	return gulp
		.src("./src/styles/**/*.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(prefix({ browsers: ["last 2 versions"] }))
		.pipe(gulp.dest("./static/styles"));
});

gulp.task("style:min", () => {
	return gulp
		.src("./src/styles/*.scss")
		.pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
		.pipe(prefix({ browsers: ["last 2 versions"] }))
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest("./static/styles"));
});

gulp.task("sass:watch", function() {
	gulp.watch("./src/styles/**/*.scss", ["style:min"]);
});

gulp.task("svg", () => {
	return gulp
		.src("./src/img/**/*.svg")
		.pipe(svgo())
		.pipe(gulp.dest("./static/img/"));
});

gulp.task("images", () => {
	return gulp
		.src(["./src/img/**/", "!./src/img/**/*.svg"])
		.pipe(gulp.dest("./static/img/"));
});

gulp.task("build:dev", ["style", "svg", "images"]);
gulp.task("build:prod", ["style:min", "svg", "images"]);
